#!/usr/bin/perl
# Begin-Doc
##################################################################
# Name: maintain_passwd.pl
# Type: perl cron script
#
# Syntax: perl script
#
# Description: Creates groups and password files for nic-cluster
#                  hosts based on a groups.conf file for each host.
#              Script runs via crontab on cluster head nodes.
# Crontab Entries for nicsoft:
#               0,15,30,45 * * * * /local/nicsoft/usermaint/opl -ga>/dev/null 2>&1
#
##################################################################
use lib "/usr/lib64/perl5/site_perl/5.8.8/x86_64-linux-thread-multi";
use lib "/usr/lib/perl5/site_perl/5.8.8";
use lib "/local/nicsoft/usermaint/lib";
use lib "/local/umrperl/libs";
use strict;

use Net::SMTP;
use File::Basename;
use UMR::AuthSrv;
use UMR::SysProg::SetUID;
use UMR::SysProg::NIS::UserGroup;
use UMR::SysProg::ADSObject;
use UMR::Env;
use usermaint;
use Getopt::Long;
use File::Copy;
my $debug        = 0;
my $date = localtime;
my $admins = "nic-cluster-admins\@mst.edu";
my $sendadminmail = 0;
my $adminmessagecontent = "User update report for $date\n";
my $deleteusers = 0;
my $options_okay = GetOptions(

    # Application-specific options...
    'debug'  => \$debug,
    'clean'  => \$deleteusers,
    'help|?' => sub { usage(); }
);
if ( !$options_okay ) {
    usage();
}
my $userid = "nicsoft";
SetUID($userid);
my $pw = AuthSrv_Fetch( user => $userid, instance => "ads" );
my $ads =
  new UMR::SysProg::ADSObject( user => $userid, password => $pw, use_gc => 1 )
  || die $UMR::Sysprog::ADSObject::ErrorMsg;
my $hostname    = $ENV{HOSTNAME};
my $hostconfigfilename;
my $env = &UMR_Env();
print "Environment = $env\n" if $debug;

if ( $env eq "dev" ) {
    $hostconfigfilename = "/local/nicsoft/usermaint/groupsdev.conf";
}
elsif ( $env eq "test" ) {
    $hostconfigfilename = "/local/nicsoft/usermaint/groupsdev.conf";
}
else {
    $hostconfigfilename = "/local/nicsoft/usermaint/groups.conf";
}
print "Should read config from $hostconfigfilename\n" if $debug;
my $HOSTCONF;
open( $HOSTCONF, "<$hostconfigfilename" )
  || die "Failed to open hosts configuration file.";
my @grouplines = <$HOSTCONF>;
close($HOSTCONF);
my $groupfilename  = "/etc/group";
my $passwdfilename = "/etc/passwd";

#Preload userid's from passwd file.
my $PASSWD;
my %uidhash;
my %newuidhash;
my @currentusers;
open( $PASSWD, "<$passwdfilename" ) || die "Cannot read $passwdfilename";
foreach my $passwdline (<$PASSWD>) {
    my ( $uname, $paswd, $idnum, $gid, $descr, $home, $shell ) =
      split( /:/, $passwdline );
    $uidhash{$uname} = $idnum;
    push( @currentusers, $uname );
    if ( $idnum < 5000 ) {
        $newuidhash{$uname} = $idnum;
    }
    if ( $idnum = 65534 ) {
	$newuidhash{$uname} = $idnum;
    }
}
close($PASSWD);

#Preload groupid's from group file.
my $GRPF;
my %gidhash;
open( $GRPF, "<$groupfilename" ) || die "Cannot read $groupfilename";
foreach my $groupline (<$GRPF>) {
    my ( $gname, $gpass, $gidnum, $members ) = split( /:/, $groupline );
    $gidhash{$gname} = $gidnum;
}
close($GRPF);
my %allusers;
my $pw = AuthSrv_Fetch( user => $userid, instance => "ads" );
my $ads =
  new UMR::SysProg::ADSObject( user => $userid, password => $pw, use_gc => 1 )
  || die $UMR::Sysprog::ADSObject::ErrorMsg;
my @groups;
foreach my $groupline (@grouplines) {
    my ( $netgroup, $usershell ) = split( / /, $groupline, 2 );
    push( @groups, $netgroup );
}
my %allmembers = NIS_UserGroup_MemberUsersMulti(@groups);
foreach my $groupline (@grouplines) {
    my ( $netgroup, $usershell ) = split( / /, $groupline, 2 );
    print "$0: Getting all members of $netgroup\n" if $debug;
    chomp($usershell);
    chomp($netgroup);

    #Get group id
    my $gid;
    if ( exists $gidhash{$netgroup} ) {
        $gid = $gidhash{$netgroup};
	# Delete group to clear membership
	if ($deleteusers) {
		sudowrap("/usr/sbin/groupdel", "$netgroup" );
		# Recreate the group as an empty group
		sudowrap( "/usr/sbin/groupadd", "-g $gid", "-f", "$netgroup" );
	}
    }
    else {
        my $grpinfo =
          $ads->GetAttributes( $netgroup, attributes => ['msSFU30GidNumber'] );
        my @grpid = @{ $grpinfo->{msSFU30GidNumber} };
        $gid = $grpid[0];
        #Create the netgroup on local system.
        sudowrap( "/usr/sbin/groupadd", "-g $gid", "-f", "$netgroup" );
    }

    #Add the user accounts.
    my @members = @{ %allmembers->{$netgroup} };
    foreach my $member (@members) {
        my $uidnum;
        if ( exists $uidhash{$member} ) {
            $uidnum = $uidhash{$member};
            $newuidhash{$member} = $uidnum;
	    sudowrap (
		"/usr/sbin/usermod",
		"-a",
		"-G",
		"$netgroup",
		"$member"
	    );
        }
        else {
            my $userinfo =
              $ads->GetAttributes( $member,
                attributes => ['msSFU30UidNumber'] );
            if ( exists $userinfo->{msSFU30UidNumber} ) {
                my @uid = @{ $userinfo->{msSFU30UidNumber} };
                $uidnum = @uid[0];
                $newuidhash{$member} = $uidnum;
		$uidhash{$member} = $uidnum;

                #Add the user
                sudowrap(
                    "/usr/sbin/adduser", "-u $uidnum",
                    "-s$usershell", "$member"
                );
		#Fix usergid
		sudowrap(
		   "/usr/sbin/groupmod", "-g $uidnum", "$member"
		);
		#Add scratch folder
		sudowrap(
		   "/bin/mkdir", "-p /scratch/$member"
		);
		sudowrap(
		  "/bin/chown", "$member:$member", "/scratch/$member"
		);
		sudowrap (
                	"/usr/sbin/usermod",
               	 	"-a",
                	"-G",
                	"$netgroup",
                	"$member"
                );
		$adminmessagecontent .= "$0: Added new user $member.\n";
		emailuserwelcome($member);
		$sendadminmail = 1;
            }
            else {
                print "Unable to identify $member\n" if $debug;
            }
        }
    }
}
##We want to cleanup nic-cluster accounts
foreach my $currentuser (@currentusers) {
    if ( exists $newuidhash{$currentuser} ) {
    }
    else {
        print "Deleting $currentuser\n" if $debug;
	$adminmessagecontent .= "$0: Deleted user $currentuser/\n";
	$sendadminmail = 1;
	
        sudowrap( "/usr/sbin/userdel", "$currentuser" );
    }
}
if ( $sendadminmail ) {
	sudowrap( "/opt/rocks/bin/rocks", "sync", "users" );
	emailadmins();
}

# Begin-Doc
# Name: usage
# Type: function
# Description: Prints the usage syntax and exits the script.
# Syntax: usage();
# End-Doc
sub usage {
    print "Usage: $0 \n";
    print "         -d|--debug\n";
    print "         -c|--clean, removes deleted users from groups.";
    print "         -h, --help displays this message\n";
    exit(0);
}

# Begin-Doc
# Name: emailadmins
# Type: function
# Description: Notifies admins about script activity.
# Syntax: emailadmins();
# End-Doc
sub emailadmins () {
	my $smtp = Net::SMTP->new('embedsmtp.mst.edu')
		or die "Can't Open server: $!";

	# For debugging purposes.
	# print $smtp->banner, "\n";
	$smtp->mail("$admins");
	$smtp->to("$admins");
	$smtp->data();
	$smtp->datasend("To: $admins\n");
	$smtp->datasend("From: $admins\n");
	$smtp->datasend("Subject: NIC Cluster User Maintenance Update\n");
	$smtp->datasend("Reply-To: $admins\n");
	$smtp->datasend("\n");
	$smtp->datasend("$adminmessagecontent\n\n\n");
	$smtp->dataend();
	$smtp->quit;
}


# Begin-Doc
# Name: emailuserwelcome
# Type: function
# Description: Sends a welcome e-mail to a new user.
# Syntax: emailuserwelcome($user);
# End-Doc
sub emailuserwelcome() {
	my $user = shift();
	my $from     = "itcomms";
	my $reply_to = "itcomms";
	my $host     = "mst.edu";
	my $subject  = "NIC Cluster access granted";
	my $content  = qq{

The account "$user" has been granted access to the NIC Cluster.  

Please take the time to go through the documentation at:

http://hpc.mst.edu

This site include code examples, and example job submission files specific to Missouri S&T's NIC Cluster.

---

Quick start instructions for submitting a cluster job are available at:

http://hpc.mst.edu/quickstart

---

Also, remember to review the information at:

http://hpc.mst.edu/accessandpolicies

To understand what the behavior of your jobs will be, and how to go about getting more time on the cluster.

---

If you have any further questions, you can contact the IT Help Desk at (573)341-4357.

Thanks!
-Missouri S&T IT Research Support Services

	};
	my $smtp = Net::SMTP->new('embedsmtp.mst.edu')
		or die "Can't Open server: $!";

	# For debugging purposes.
	# print $smtp->banner, "\n";
	$smtp->mail("$user\@mst.edu");
	$smtp->to("$user\@mst.edu");

	$smtp->data();
	$smtp->datasend("To: $user\@$host\n");
	$smtp->datasend("From: $from\@$host\n");
	$smtp->datasend("Subject: $subject\n");
	$smtp->datasend("Reply-To: $reply_to\@mst.edu\n");
	$smtp->datasend("\n");
	$smtp->datasend("$content\n");
	$smtp->dataend();
	$smtp->quit;
}

