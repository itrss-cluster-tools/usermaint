
=begin
Begin-Doc
Name: usermaint
Type: module
Description: Holds settings and common routines used between various user maintenance scripts.
End-Doc
=cut

package usermaint;
require 5.000;
require Exporter;
use strict;
use Digest::MD5 qw(md5_hex);
use Sys::Hostname;
use IPC::Open3;
use MST::Env;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
	syswrap
	sudowrap
	setdebug
	arcdirpath
	homedirpath
	homedrive
	useroverquota
	$fsroot
	$homeroot
	$arcroot
	$headnode
	$fileserver
	$blockcount
	$filecount
	$hblocks
	$sblocks
	$hinodes
	$sinodes
	$admins
	$vgname
	$maxblockcount
	$debug
	$exportnets
);

our $fsroot;
our $homeroot;
our $arcroot;
our $admins;
our $headnode;
our $fileserver;
our $blockcount;
our $filecount;
our $hblocks;
our $sblocks;
our $hinodes;
our $sinodes;
our $debug;
our $vgname;
our $maxblockcount;
our $exportnets;

# Begin-Doc
# Name: init
# Type: function
# Description: Initialize package variables for export.
# Syntax: init();
# End-Doc
sub init {
	my $env = &MST_Env();
	if ( $env = "prod" ) {
		print "Initializing variables.....\n" if $debug;
		$fsroot     = "/data";
		$homeroot   = "$fsroot/users";
		$arcroot    = "$fsroot/users-old";
		$admins     = "nic-cluster-admins\@mst.edu";
		$headnode   = "nic-p2.srv.mst.edu";
		$fileserver = "nicfs-gen.local";
		$blockcount = 1000000;
		$filecount  = 100000;
		$hblocks    = $blockcount;
		$sblocks    = sprintf( "%d", $hblocks * 0.90 );
		$hinodes    = $filecount;
		$sinodes    = sprintf( "%d", $hinodes * 0.90 );
		$vgname     = "nicvg";

		#Maximum quota before create of virtual disk.
		$maxblockcount = 10000000;
		$debug         = 0;
		$exportnets    = "10.2.5.0/255.255.255.0";
	}

	elsif ( $env = "dev") {
		print "Initializing variables.....\n" if $debug;
		$fsroot     = "/export";
		$homeroot   = "$fsroot/homes";
		$arcroot    = "$fsroot/users";
		$admins     = "nic-cluster-admins\@mst.edu";
		$headnode   = "nic-d1.srv.mst.edu";
		$fileserver = "nas-0-0.nic-dev.mst.edu";
		$blockcount = 100000;
		$filecount  = 10000;
		$hblocks    = $blockcount;
		$sblocks    = sprintf( "%d", $hblocks * 0.90 );
		$hinodes    = $filecount;
		$sinodes    = sprintf( "%d", $hinodes * 0.90 );
		$vgname     = "main";

		#Maximum quota before create of virtual disk.
		$maxblockcount = 1000000;
		$debug         = 0;
		$exportnets    = "10.2.6.48/255.255.255.248";
	}
	else {
		die
			"Unable to determine environment script is exiting.\n";
	}
}

# Begin-Doc
# Name: setdebug
# Type: function
# Description: A debugging wrapper for system calls that prints
#              what would have been called if debugging were off.
# Syntax: setdebug();
# End-Doc
sub setdebug {
	$debug = 1;
}

# Begin-Doc
# Name: syswrap
# Type: function
# Description: A debugging wrapper for system calls that prints
#              what would have been called if debugging were off.
# Syntax: syswrap(@system_parameters);
# End-Doc
sub syswrap {
	my @system_parameters = @_;
	if ($debug) {
		print "Executing: @system_parameters\n";
		print "Debug: @system_parameters";
		print "\n";
	}
	else {
		system(@system_parameters);
	}
}

# Begin-Doc
# Name: sudowrap
# Type: function
# Description: A debugging wrapper for sudo system calls that prints
#              what would have been called if debugging were off.
# Syntax: sudowrap(@system_parameters);
# End-Doc
sub sudowrap {
	my @system_parameters = @_;
	if ($debug) {
		print "Executing: @system_parameters\n";
		print "Debug: @system_parameters";
		print "\n";
	}
	else {
		#print "Running sudo @system_parameters\n";
		system("sudo", @system_parameters);
	}
}

# Begin-Doc
# Name: homedirpath
# Type: function
# Description: Calculate and return the full path to the users home
#              directory.
# Syntax: homedirpath(username);
# End-Doc
sub homedirpath {
	my $username   = shift();
	my $userhash   = md5_hex($username);
	my $usersubdir = substr( $userhash, 0, 1 );
	return "$homeroot/$usersubdir/$username";
}

# Begin-Doc
# Name: arcdirpath
# Type: function
# Description: Calculate and return the full path to the user archive
#              directory.
# Syntax: arcdirpath(username);
# End-Doc
sub arcdirpath {
	my $username = shift();
	return "$arcroot/$username";
}

# Begin-Doc
# Name: homedrive
# Type: function
# Description: Calculate and return the logical volume mount point
#              for a users home directory.
# Syntax: homedrive(username);
# End-Doc
sub homedrive {
	my $username   = shift();
	my $userhash   = md5_hex($username);
	my $usersubdir = substr( $userhash, 0, 1 );
	return "$homeroot/$usersubdir";
}

# Begin-Doc
# Name: homedrive
# Type: function
# Description: Determine if a user has exceeded 90% quota utilization.
# Syntax: useroverquota(username, uid);
# Returns: 0 if under quota 1 if over quota.
# End-Doc
sub useroverquota {
	my $username = shift();
	my $uid = shift();
	if ( $username ne "" ) {
		if ( -e "/dev/mapper/$vgname-nicfs--users--$username" ) {
			#User has private logical Volume
			open3( my $wtr, my $cmdoutput,
				"", "/bin/df", "-kh",
				"/dev/mapper/$vgname-nicfs--users--$username" );
			close($wtr);
			while ( my $line = <$cmdoutput> ) {
				if ( $line =~ m/\d%/ ) {
					my @values = split( / /, $line );
					foreach my $value (@values) {
						if ( $value =~ m/\d%/ ) {
							my $percentused = 0;
							$percentused = $value;
							$percentused =~ s/%//;
							if ( $percentused > 90 ) {
								return 1;
							}
						}
					}
				}
			}
			close ($cmdoutput);
		}
		else {
			# Users has directory on shared volume.
			open3 (my $wtr, my $cmdout, "", "/usr/bin/quota", "-s", "$uid" );
			close($wtr);
			while ( my $line = <$cmdout> ) {
				if ( $line =~ m/\*/ ) {
					return 1;
				}
			}
		}
	}
	return 0;
}

init();
1;
